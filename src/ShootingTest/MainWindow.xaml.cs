﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ShootingTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ViewModel vm;
        Bullet bullet;
        DispatcherTimer bulletMoveTimer = new DispatcherTimer();

        public MainWindow()
        {
            InitializeComponent();
            vm = new ViewModel();
            DataContext = vm;

            bulletMoveTimer.Interval = new TimeSpan(0, 0, 0, 0, 30);
            bulletMoveTimer.Tick += BulletMoveTimer_Tick;
            bulletMoveTimer.Start();
        }

        private void BulletMoveTimer_Tick(object sender, EventArgs e)
        {
            if (bullet == null)
                return;

            bullet.Move();
        }

        private void SetButton_Click(object sender, RoutedEventArgs rea)
        {
            canvas.Children.Clear();
            DrawPoint(vm.MyX, vm.MyY, Brushes.Green);
            DrawPoint(vm.TargetX, vm.TargetY, Brushes.Red);
        }
        private void DrawLineButton_Click(object sender, RoutedEventArgs e)
        {
            DrawLine(vm.MyX, vm.TargetX, vm.MyY, vm.TargetY);
        }
        private void DrawPoint(int x, int y, Brush color)
        {
            Ellipse e = new Ellipse();
            e.Height = 10;
            e.Width = 10;
            e.Fill = color;
            e.Stroke = color;
            Canvas.SetLeft(e, x);
            Canvas.SetTop(e, y);
            canvas.Children.Add(e);
        }
        private void DrawLine(int fromX, int toX, int fromY, int toY)
        {
            Line line = new Line();
            line.X1 = fromX + 5;
            line.X2 = toX + 5;
            line.Y1 = fromY + 5;
            line.Y2 = toY + 5;
            //line.Width = 10;
            line.Fill = Brushes.Black;
            line.Stroke = Brushes.Black;
            canvas.Children.Add(line);
        }
        private void ShootButton_Click(object sender, RoutedEventArgs e)
        {
            if (bullet != null)
                canvas.Children.Remove(bullet.Ellipse);

            bullet = new Bullet(vm.MyX, vm.MyY, 3 * Math.Cos(vm.Angle * Math.PI / 180), 3 * Math.Sin(vm.Angle * Math.PI / 180));
            Canvas.SetLeft(bullet.Ellipse, bullet.X);
            Canvas.SetTop(bullet.Ellipse, bullet.Y);
            canvas.Children.Add(bullet.Ellipse);
        }

        private void CalculateAngleButton_Click(object sender, RoutedEventArgs e)
        {
            vm.CalculateAngle();
        }
    }
}